import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TodoService {

  private url: string;
  constructor(private http: HttpClient) {
  	this.url = 'https://localhost:5001/api/todo';
  }

  index(){
  	return this.http.get(this.url);
  }

  create(data){
  	return this.http.post(this.url, data);
  }

  update(data) {
  	return this.http.put(this.url + "/" + data.id, data);
  }

  delete(id) {
  	return this.http.delete(this.url + "/" + id);
  }
}
